use std::env;
use std::path::{PathBuf};
use std::process::{Command, Stdio};

fn run(cmd: &mut Command) {
  //println!("running: {:?}", cmd);
  let res = cmd.stdout(Stdio::inherit())
               .stderr(Stdio::inherit())
               .status()
               .unwrap()
               .success();
  assert!(res);
}

fn main() {
  let root = PathBuf::from(&env::var("CARGO_MANIFEST_DIR").unwrap());
  let dst = PathBuf::from(&env::var("OUT_DIR").unwrap());

  let mut mdb_root = root.clone();
  for sub_dir in vec!["lmdb", "libraries", "liblmdb"].iter() {
    mdb_root.push(sub_dir)
  }

  let lib_dir = dst.clone();

  let mut clean_cmd = Command::new("make");
  clean_cmd.arg("-C").arg(&mdb_root);
  clean_cmd.arg("clean");
  run(&mut clean_cmd);

  let mut build_cmd = Command::new("make");
  build_cmd.arg("-C").arg(&mdb_root);
  build_cmd.arg("liblmdb.a");
  run(&mut build_cmd);

  run(Command::new("cp")
      .arg(&mdb_root.join("liblmdb.a"))
      .arg(&lib_dir.join("liblmdb_native.a")));

  println!("cargo:rustc-flags=-L {} -l static=lmdb_native", lib_dir.display());
}
