//#![feature(libc)]
#![feature(raw_ext)]

extern crate libc;

pub mod ffi {

use libc::{c_void, c_char, c_int, c_uint, size_t};
use std::os::linux::raw::{mode_t};

pub type mdb_mode_t = mode_t;
pub type mdb_filehandle_t = c_int;

#[repr(C)]
pub struct MDB_env;

#[derive(Default)]
#[repr(C)]
pub struct MDB_stat {
  pub ms_psize:           c_uint,
  pub ms_depth:           c_uint,
  pub ms_branch_pages:    size_t,
  pub ms_leaf_pages:      size_t,
  pub ms_overflow_pages:  size_t,
  pub ms_entries:         size_t,
}

#[repr(C)]
pub struct MDB_txn;

pub type MDB_dbi = c_uint;

#[repr(C)]
pub struct MDB_cursor;

#[repr(C)]
pub enum MDB_cursor_op {
  MDB_FIRST           = 0,
  MDB_FIRST_DUP       = 1,
  MDB_GET_BOTH        = 2,
  MDB_GET_BOTH_RANGE  = 3,
  MDB_GET_CURRENT     = 4,
  MDB_GET_MULTIPLE    = 5,
  MDB_LAST            = 6,
  MDB_LAST_DUP        = 7,
  MDB_NEXT            = 8,
  MDB_NEXT_DUP        = 9,
  MDB_NEXT_MULTIPLE   = 10,
  MDB_NEXT_NODUP      = 11,
  MDB_PREV            = 12,
  MDB_PREV_DUP        = 13,
  MDB_PREV_NODUP      = 14,
  MDB_SET             = 15,
  MDB_SET_KEY         = 16,
  MDB_SET_RANGE       = 17,
}

#[repr(C)]
pub struct MDB_val {
  pub mv_size: size_t,
  pub mv_data: *mut c_void,
}

// Environment flags.
pub const MDB_FIXEDMAP:   u32 = 0x01;
pub const MDB_NOSUBDIR:   u32 = 0x4000;
pub const MDB_NOSYNC:     u32 = 0x10000;
pub const MDB_RDONLY:     u32 = 0x20000;
pub const MDB_NOMETASYNC: u32 = 0x40000;
pub const MDB_WRITEMAP:   u32 = 0x80000;
pub const MDB_MAPASYNC:   u32 = 0x100000;
pub const MDB_NOTLS:      u32 = 0x200000;
pub const MDB_NOLOCK:     u32 = 0x400000;
pub const MDB_NORDAHEAD:  u32 = 0x800000;
pub const MDB_NONMEMINIT: u32 = 0x1000000;

// Database flags.
pub const MDB_REVERSEKEY: u32 = 0x02;
pub const MDB_DUPSORT:    u32 = 0x04;
pub const MDB_INTEGERKEY: u32 = 0x08;
pub const MDB_DUPFIXED:   u32 = 0x10;
pub const MDB_INTEGERDUP: u32 = 0x20;
pub const MDB_REVERSEDUP: u32 = 0x40;
pub const MDB_CREATE:     u32 = 0x40000;

// Write flags.
// TODO

// Copy flags.
// TODO

extern "C" {
  pub fn mdb_version(major: *mut c_int, minor: *mut c_int, patch: *mut c_int) -> *mut c_char;
  pub fn mdb_strerror(err: c_int) -> *mut c_char;
  pub fn mdb_env_create(env: *mut *mut MDB_env) -> c_int;
  pub fn mdb_env_open(env: *mut MDB_env, path: *const c_char, flags: c_uint, mode: mdb_mode_t) -> c_int;
  pub fn mdb_env_close(env: *mut MDB_env);
  pub fn mdb_env_set_mapsize(env: *mut MDB_env, size: size_t) -> c_int;
  pub fn mdb_env_stat(env: *mut MDB_env, stat: *mut MDB_stat) -> c_int;

  pub fn mdb_txn_begin(env: *mut MDB_env, parent: *mut MDB_txn, flags: c_uint, txn: *mut *mut MDB_txn) -> c_int;
  pub fn mdb_txn_commit(txn: *mut MDB_txn) -> c_int;
  pub fn mdb_txn_abort(txn: *mut MDB_txn);
  pub fn mdb_txn_reset(txn: *mut MDB_txn);
  pub fn mdb_txn_renew(txn: *mut MDB_txn) -> c_int;

  pub fn mdb_dbi_open(txn: *mut MDB_txn, name: *const c_char, flags: c_uint, dbi: *mut MDB_dbi) -> c_int;
  pub fn mdb_dbi_close(env: *mut MDB_env, dbi: MDB_dbi);

  pub fn mdb_cursor_open(txn: *mut MDB_txn, dbi: MDB_dbi, cursor: *mut *mut MDB_cursor) -> c_int;
  pub fn mdb_cursor_close(cursor: *mut MDB_cursor);
  pub fn mdb_cursor_get(cursor: *mut MDB_cursor, key: *mut MDB_val, data: *mut MDB_val, op: c_int) -> c_int;
}

}
