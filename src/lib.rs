#![feature(unique)]

extern crate libc;
extern crate lmdb_sys;

use libc::{c_int, size_t};
use lmdb_sys::ffi::*;

use std::ffi::{CString, OsStr};
use std::mem::{transmute};
use std::path::{Path};
use std::ptr::{Unique, null_mut};
use std::rc::{Rc};
use std::slice::{from_raw_parts};

pub struct LmdbStat {
  stat: MDB_stat,
}

impl LmdbStat {
  pub fn entries(&self) -> usize {
    self.stat.ms_entries as usize
  }
}

pub struct LmdbEnv {
  env_ptr: Unique<MDB_env>,
}

impl LmdbEnv {
  pub fn open_read_only(path: &Path) -> Result<LmdbEnv, ()> {
    let mut env_ptr: *mut MDB_env = null_mut();
    match unsafe { mdb_env_create(&mut env_ptr as *mut *mut MDB_env) } {
      0 => {}
      _ => return Err(()),
    }
    //#define CHANGELESS      (MDB_FIXEDMAP|MDB_NOSUBDIR|MDB_RDONLY| \
    //        MDB_WRITEMAP|MDB_NOTLS|MDB_NOLOCK|MDB_NORDAHEAD)
    let flags = MDB_RDONLY | MDB_NOLOCK;
    let path_os: &OsStr = path.as_ref();
    let path_cstr = CString::new(path_os.to_str().unwrap().as_bytes().to_vec())
      .ok().expect("failed to make C string!");
    match unsafe { mdb_env_open(env_ptr, path_cstr.as_ptr(), flags, 0o444) } {
      0 => {}
      _ => return Err(()),
    }
    Ok(LmdbEnv{env_ptr: unsafe { Unique::new(env_ptr) }})
  }

  pub fn set_map_size(&mut self, map_size: u64) -> Result<(), ()> {
    match unsafe { mdb_env_set_mapsize(self.env_ptr.offset(0), map_size as size_t) } {
      0 => Ok(()),
      _ => Err(()),
    }
  }

  pub fn stat(&mut self) -> Result<LmdbStat, ()> {
    let mut stat: MDB_stat = Default::default();
    match unsafe { mdb_env_stat(self.env_ptr.offset(0), &mut stat as *mut MDB_stat) } {
      0 => Ok(LmdbStat{stat: stat}),
      _ => Err(()),
    }
  }
}

impl Drop for LmdbEnv {
  fn drop(&mut self) {
    unsafe { mdb_env_close(self.env_ptr.offset(0)) };
    // TODO: destroy MDB env?
  }
}

pub struct LmdbCursor<'env> {
  env:        &'env LmdbEnv,
  txn_ptr:    *mut MDB_txn,
  dbi:        MDB_dbi,
  cursor_ptr: *mut MDB_cursor,
}

impl<'env> LmdbCursor<'env> {
  pub fn new_read_only(env: &'env LmdbEnv) -> Result<LmdbCursor<'env>, ()> {
    let mut txn_ptr: *mut MDB_txn = null_mut();
    match unsafe { mdb_txn_begin(env.env_ptr.offset(0), null_mut(), MDB_RDONLY, &mut txn_ptr as *mut *mut MDB_txn) } {
      0 => {}
      _ => return Err(()),
    }
    let mut dbi: MDB_dbi = 0;
    match unsafe { mdb_dbi_open(txn_ptr, null_mut(), 0, &mut dbi as *mut MDB_dbi) } {
      0 => {}
      _ => return Err(()),
    }
    let mut cursor_ptr: *mut MDB_cursor = null_mut();
    match unsafe { mdb_cursor_open(txn_ptr, dbi, &mut cursor_ptr as *mut *mut MDB_cursor) } {
      0 => {}
      _ => return Err(()),
    }
    Ok(LmdbCursor{
      env:        env,
      txn_ptr:    txn_ptr,
      dbi:        dbi,
      cursor_ptr: cursor_ptr,
    })
  }

  pub fn iter(&'env self) -> LmdbCursorIterator<'env> {
    LmdbCursorIterator{
      cursor:   self,
      start_key:    None,
      key:      MDB_val{mv_size: 0, mv_data: null_mut()},
      value:    MDB_val{mv_size: 0, mv_data: null_mut()},
      init:     false,
    }
  }

  pub fn seek_iter(&'env self, start_key: Vec<u8>) -> LmdbCursorIterator<'env> {
    LmdbCursorIterator{
      cursor:   self,
      start_key:    Some(start_key),
      key:      MDB_val{mv_size: 0, mv_data: null_mut()},
      value:    MDB_val{mv_size: 0, mv_data: null_mut()},
      init:     false,
    }
  }
}

impl<'env> Drop for LmdbCursor<'env> {
  fn drop(&mut self) {
    unsafe { mdb_cursor_close(self.cursor_ptr) };
    unsafe { mdb_dbi_close(self.env.env_ptr.offset(0), self.dbi) };
    unsafe { mdb_txn_abort(self.txn_ptr) };
  }
}

pub struct LmdbCursorItem<'env> {
  pub key:    &'env [u8],
  pub value:  &'env [u8],
}

pub struct LmdbCursorIterator<'env> {
  cursor:   &'env LmdbCursor<'env>,
  start_key:    Option<Vec<u8>>,
  key:      MDB_val,
  value:    MDB_val,
  init:     bool,
}

impl<'env> Iterator for LmdbCursorIterator<'env> {
  type Item = LmdbCursorItem<'env>;

  fn next(&mut self) -> Option<LmdbCursorItem<'env>> {
    let op = match self.init {
      false => {
        self.init = true;
        match self.start_key {
          None => {
            MDB_cursor_op::MDB_FIRST
          }
          Some(ref start_key) => {
            // XXX(20160413): Set the key info to the specified key.
            self.key.mv_size = start_key.len();
            self.key.mv_data = unsafe { transmute(start_key.as_ptr()) };
            MDB_cursor_op::MDB_SET_RANGE
          }
        }
      }
      true => MDB_cursor_op::MDB_NEXT,
    };
    match unsafe { mdb_cursor_get(self.cursor.cursor_ptr, &mut self.key as *mut MDB_val, &mut self.value as *mut MDB_val, op as c_int) } {
      0 => {}
      _ => return None,
    }
    // FIXME: should keep a copy of this in ourselves, and return a ref.
    Some(LmdbCursorItem{
      key:    unsafe { from_raw_parts(self.key.mv_data as *const u8, self.key.mv_size as usize) },
      value:  unsafe { from_raw_parts(self.value.mv_data as *const u8, self.value.mv_size as usize) },
    })
  }
}

pub struct LmdbRcCursor {
  env:        Rc<LmdbEnv>,
  txn_ptr:    *mut MDB_txn,
  dbi:        MDB_dbi,
  cursor_ptr: *mut MDB_cursor,
}

impl LmdbRcCursor {
  pub fn new_read_only(env: Rc<LmdbEnv>) -> Result<LmdbRcCursor, ()> {
    let mut txn_ptr: *mut MDB_txn = null_mut();
    match unsafe { mdb_txn_begin(env.env_ptr.offset(0), null_mut(), MDB_RDONLY, &mut txn_ptr as *mut *mut MDB_txn) } {
      0 => {}
      _ => return Err(()),
    }
    let mut dbi: MDB_dbi = 0;
    match unsafe { mdb_dbi_open(txn_ptr, null_mut(), 0, &mut dbi as *mut MDB_dbi) } {
      0 => {}
      _ => return Err(()),
    }
    let mut cursor_ptr: *mut MDB_cursor = null_mut();
    match unsafe { mdb_cursor_open(txn_ptr, dbi, &mut cursor_ptr as *mut *mut MDB_cursor) } {
      0 => {}
      _ => return Err(()),
    }
    Ok(LmdbRcCursor{
      env:        env,
      txn_ptr:    txn_ptr,
      dbi:        dbi,
      cursor_ptr: cursor_ptr,
    })
  }

  pub fn iter(cursor: Rc<LmdbRcCursor>) -> LmdbRcCursorIterator {
    LmdbRcCursorIterator{
      cursor:   cursor,
      start_key:    None,
      key:      MDB_val{mv_size: 0, mv_data: null_mut()},
      value:    MDB_val{mv_size: 0, mv_data: null_mut()},
      init:     false,
    }
  }

  pub fn seek_iter(cursor: Rc<LmdbRcCursor>, start_key: Vec<u8>) -> LmdbRcCursorIterator {
    LmdbRcCursorIterator{
      cursor:   cursor,
      start_key:    Some(start_key),
      key:      MDB_val{mv_size: 0, mv_data: null_mut()},
      value:    MDB_val{mv_size: 0, mv_data: null_mut()},
      init:     false,
    }
  }
}

impl Drop for LmdbRcCursor {
  fn drop(&mut self) {
    unsafe { mdb_cursor_close(self.cursor_ptr) };
    unsafe { mdb_dbi_close(self.env.env_ptr.offset(0), self.dbi) };
    unsafe { mdb_txn_abort(self.txn_ptr) };
  }
}

pub struct LmdbOwnedCursorItem {
  pub key:    Vec<u8>,
  pub value:  Vec<u8>,
}

pub struct LmdbRcCursorIterator {
  cursor:   Rc<LmdbRcCursor>,
  start_key:    Option<Vec<u8>>,
  key:      MDB_val,
  value:    MDB_val,
  init:     bool,
}

impl Iterator for LmdbRcCursorIterator {
  type Item = LmdbOwnedCursorItem;

  fn next(&mut self) -> Option<LmdbOwnedCursorItem> {
    let op = match self.init {
      false => {
        self.init = true;
        match self.start_key {
          None => {
            MDB_cursor_op::MDB_FIRST
          }
          Some(ref start_key) => {
            // XXX(20160413): Set the key info to the specified key.
            self.key.mv_size = start_key.len();
            self.key.mv_data = unsafe { transmute(start_key.as_ptr()) };
            MDB_cursor_op::MDB_SET_RANGE
          }
        }
      }
      true => MDB_cursor_op::MDB_NEXT,
    };
    match unsafe { mdb_cursor_get(self.cursor.cursor_ptr, &mut self.key as *mut MDB_val, &mut self.value as *mut MDB_val, op as c_int) } {
      0 => {}
      _ => return None,
    }
    // FIXME: should keep a copy of this in ourselves, and return a ref.
    Some(LmdbOwnedCursorItem{
      key:    unsafe { from_raw_parts(self.key.mv_data as *const u8, self.key.mv_size as usize) }.to_vec(),
      value:  unsafe { from_raw_parts(self.value.mv_data as *const u8, self.value.mv_size as usize) }.to_vec(),
    })
  }
}
